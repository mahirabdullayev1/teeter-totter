const detectCollisionPoint = (
  balanceLineTopPosition,
  rotationDegreeOfBalance,
  positionProbability,
  balanceSide = "left"
) => {
  const positionYDelta = Math.abs(
    Math.sin(degrees_to_radians(rotationDegreeOfBalance)) * 500
  );
  const increase =
    parseInt(balanceLineTopPosition) +
    positionYDelta * (1 - positionProbability);
  const decrease =
    parseInt(balanceLineTopPosition) -
    positionYDelta * (1 - positionProbability);
  return (balanceSide === "left" && rotationDegreeOfBalance > 0) ||
    (balanceSide === "right" && rotationDegreeOfBalance < 0)
    ? decrease
    : increase;
};

const degrees_to_radians = (degrees) => degrees * (Math.PI / 180);

export { detectCollisionPoint };
