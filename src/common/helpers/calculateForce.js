const calculateForce = (rightWeights, leftWeights) => {
  let rightForce = 0,
    leftForce = 0;
  for (let weight of leftWeights) {
    if (weight.isFalling) continue;
    leftForce += weight.weight * (1 - weight.horizontalPositionProbability);
  }
  for (let weight of rightWeights) {
    if (weight.isFalling) continue;
    rightForce += weight.weight * (1 - weight.horizontalPositionProbability);
  }

  return { rightForce, leftForce };
};

export { calculateForce };
