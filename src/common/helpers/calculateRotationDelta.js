import { BALANCE_FRICTION } from "@/common/constants/game";
const calculateRotationDelta = (rightForce, leftForce) => {
  if (!leftForce || !rightForce) return 0;
  return rightForce > leftForce
    ? (rightForce / leftForce) * BALANCE_FRICTION
    : (leftForce / rightForce) * -1 * BALANCE_FRICTION;
};
export { calculateRotationDelta };
